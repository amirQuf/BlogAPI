# BlogAPI
this is a __blog API__  using __Django__  and __Django rest framework__

##### How to run the Project?

```bash
python3 -m venv .venv 
source .venv/bin/activate
pip install -r requierment.txt
python3 manage.py runserver
```

### requirements:

* Python
* Django
* Django rest framework

## APPS:

* users : to handle some models that related to models

* Web : include models like Post ,Comment ,Category , Like , SavedPost

  

## Models:

* Post ,Comment ,Category , Like , SavedPost  in web APP
* Profile  , Followers in users APP

### Todo:

- [x] ~~adding fork ability for Blogs~~
- [ ] add OTP
- [x] ~~adding .env file for settings~~
- [ ] write beautiful front (maybe)
- [ ] writing tests
- [x] ~~removing api ~~
- [ ] update readme

